package com.example.application.views.übersicht;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class ÜbersichtViewTest {

    @Test
    public void shouldReturnCorrectDate() {
        ÜbersichtView uebersicht = new ÜbersichtView();
        String expectedDate = String.valueOf(LocalDate.now());
        String realDate = uebersicht.getDate();

        Assert.assertEquals(realDate, expectedDate);
    }
}