package com.example.application.views.küche;

import com.vaadin.flow.component.Text;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

class KuecheViewTest {
    @Test
        public void checkDatum() {
            KuecheView kueche = new KuecheView();
            String expectedDatum = "Heute, " + LocalDate.now();
            Text realesDatum = kueche.getDate();

            Assert.assertEquals(realesDatum,expectedDatum);
        }
}
