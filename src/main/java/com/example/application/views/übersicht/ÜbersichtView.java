package com.example.application.views.übersicht;

import com.example.application.views.main.MainView;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import java.time.LocalDate;

@Route(value = "uebersicht", layout = MainView.class)
@RouteAlias(value = "", layout = MainView.class)
@PageTitle("Übersicht")
public class ÜbersichtView extends Div {

    public String getDate() {
        return String.valueOf(LocalDate.now());
    }
    public ÜbersichtView() {
        addClassNames("übersicht-view","py-l","px-l");

        Span s = new Span("Heute " + getDate());
        s.getElement().getStyle().set("font-size", "24px").set("font-weight", "bold");
        add(s);
/*
        HorizontalLayout layout = new HorizontalLayout();
        layout.addClassNames("flex", "flex-col", "h-full", "cardcontainer");
        Div card1 = new Div(new Text("Info 1"));
        Div card2 = new Div(new Text("Info 2"));
        Div card3 = new Div(new Text("Info 3"));

        layout.add(card1, card2, card3);
        add(layout);
*/
        Html h = new Html("<div class='cardcontainer'>" +
                "<div><span class='cardemoji'>\uD83D\uDCCD</span><p class='cardtext-small'>Auslieferung</p><p class='cardtext-big' style='color: green;'>Fertig ✓</p></div>" +
                "<div><span class='cardemoji'>\uD83E\uDDD3\uD83C\uDFFB</span><p class='cardtext-small'>Anzahl Klienten</p><p class='cardtext-big'>7</p></div>" +
                "<div><span class='cardemoji'>\uD83D\uDE97</span><p class='cardtext-small'>Fahrer/In</p><p class='cardtext-big'>Peter Huber<span class='card-zusatzinfo'>+43 664 123 4567</span></p></div>" +
                "<div><span class='cardemoji'>\uD83C\uDF74</span><p class='cardtext-small'>Hauptspeise</p><p class='cardtext-big'>Marillenknödel</p></div>" +
                "</div>");
        add(h);
    }

}
