package com.example.application.views.küche;

import com.example.application.views.main.MainView;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import java.time.LocalDate;

@Route(value = "kueche", layout = MainView.class)
@PageTitle("Küche")
public class KuecheView extends Div {

    public Text getDate() {
        return new Text("Heute, " + LocalDate.now());
    }

    public KuecheView() {


        addClassNames("küche-view","py-l","px-l");

        /*HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setClassName("w-full flex-wrap mb-m");
        buttonLayout.setSpacing(true);
        buttonLayout.add(new Button("Montag"),
                new Button("Dienstag"),
                new Button("Mittwoch"),
                new Button("Donnerstag"),
                new Button("Freitag"));
        add(buttonLayout);
*/
        Text datum = getDate();
        Div s = new Div(datum);
        s.getElement().getStyle().set("font-size", "24px").set("font-weight", "bold");
        add(s);

        DatePicker valueDatePicker = new DatePicker();
        LocalDate now = LocalDate.now();
        valueDatePicker.setValue(now);
        valueDatePicker.getStyle().set("margin-bottom", "20px");

        valueDatePicker.addValueChangeListener(event -> {
            if (event.getValue() == null) {
                datum.setText("Wähle ein Datum");
            } else {
                datum.setText("" + event.getValue());
            }
        });

        add(valueDatePicker);


        Html h = new Html("<div class='cardcontainer'>" +
                "<div><span class='cardemoji'>\uD83C\uDF74</span><p class='cardtext-small'>Anzahl Gerichte</p><p class='cardtext-big'>7</p></div>" +
                "<div><span class='cardemoji'>\uD83D\uDEAB</span><p class='cardtext-small'>Allergien</p><p class='cardtext-big' style='color: #b70f0b'>Erdnüsse</p></div>" +
                "<div><span class='cardemoji'>\uD83D\uDE97</span><p class='cardtext-small'>Fahrer/In</p><p class='cardtext-big'>Peter Huber<span class='card-zusatzinfo'>+43 664 123 4567</span></p></div>" +
                "<div><span class='cardemoji'>\uD83C\uDF82</span><p class='cardtext-small'>Geburtstage</p><p class='cardtext-big'>Günther Maier<span class='card-zusatzinfo'>Wiener Schnitzel mit Pommes</span></p></div>" +
                "</div>");
        add(h);

        VerticalLayout textfields = new VerticalLayout();

        TextField vorspeise = new TextField("Vorspeise");
        TextField hauptspeise = new TextField("Hauptspeise");
        TextField dessert = new TextField("Dessert");

        vorspeise.addClassName("pt-0");
        vorspeise.setWidth("400px");

        hauptspeise.addClassName("pt-0");
        hauptspeise.setWidth("400px");

        dessert.addClassName("pt-0");
        dessert.setWidth("400px");

        textfields.add(vorspeise, hauptspeise, dessert);
        textfields.setClassName("p-0 pt-m pb-m mt-l");
        add(textfields);

        Button save = new Button("Speichern");
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        add(save);
    }

}
