package com.example.application.views.fahrer;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.PageTitle;
import com.example.application.views.main.MainView;

@Route(value = "fahrer", layout = MainView.class)
@PageTitle("Fahrer")
public class FahrerView extends Div {

    public FahrerView() {
        addClassName("fahrer-view");

        HorizontalLayout fahrerlayout = new HorizontalLayout();
        fahrerlayout.addClassName("splitview");

        Image image = new Image("images/maps.png", "Maps");
        image.addClassName("maps");

        VerticalLayout fahrerliste = new VerticalLayout();
        fahrerliste.addClassNames("py-l","px-l");
        fahrerliste.add(new Text("Meine Route"));

        fahrerlayout.add(image, fahrerliste);
        add(fahrerlayout);
    }

}
